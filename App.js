import React from'react'
import { StyleSheet, Text, view } from 'react-native';
import 'react-native-gesture-handler'
import Cuaca from './cuaca/Screens/index'


export default function App() {
    return (
        <Cuaca/>
        
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        color:'white',
        alignItems:'center',
        justifyContent:'center'
    }
})